import styleCard from "./Card.module.css";

const Card = (props) => {
  return (
    <div className={`${styleCard.card} ${props.className}`}>
      {props.children}
    </div>
  );
};

export default Card;
