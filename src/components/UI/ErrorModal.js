import Button from "./Button";
import Card from "./Card";
import styleErrorModal from "./ErrorModal.module.css";
import ReactDOM from "react-dom";
// import { ReactDOM } from "react";

const Backdrop = (props) => {
  return <div className={styleErrorModal.backdrop} onClick={props.onConfirm} />;
};

const ModalOverlay = (props) => {
  return (
    <Card className={styleErrorModal.modal}>
      <header className={styleErrorModal.header}>
        <h2>{props.title || "Error Modal"}</h2>
      </header>
      <div className={styleErrorModal.content}>
        <p>{props.message || "Error Message"}</p>
      </div>
      <footer className={styleErrorModal.actions}>
        <Button handlerOnClick={props.onConfirm}>Okay</Button>
      </footer>
    </Card>
  );
};

const ErrorModal = (props) => {
  return (
    <>
      {ReactDOM.createPortal(
        <Backdrop onConfirm={props.onConfirm} />,
        document.getElementById("backdrop-root")
      )}
      {ReactDOM.createPortal(
        <ModalOverlay
          title={props.title}
          message={props.message}
          onConfirm={props.onConfirm}
        />,
        document.getElementById("overlay-root")
      )}
    </>
  );
};

export default ErrorModal;
