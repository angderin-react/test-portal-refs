import styleButton from "./Button.module.css";

const Button = (props) => {
  return (
    <button
      className={styleButton.button}
      type={props.type || "button"}
      onClick={props.handlerOnClick}
    >
      {props.children}
    </button>
  );
};

export default Button;
