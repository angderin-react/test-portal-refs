import Card from "../UI/Card";
import styleListUser from "./ListUser.module.css";

const ListUser = (props) => {
  const viewListUser = props.users.map((user) => {
    return (
      <li key={user.id}>
        {user.username} ({user.age} years old)
      </li>
    );
  });

  return (
    <Card className={styleListUser.users}>
      <ul>{viewListUser}</ul>
    </Card>
  );
};
export default ListUser;
