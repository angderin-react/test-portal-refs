import React, { useRef, useState } from "react";

import Button from "../UI/Button";
import Card from "../UI/Card";
import ErrorModal from "../UI/ErrorModal";

import styleAddUser from "./AddUser.module.css";

const AddUser = (props) => {
  const nameInputRef = useRef();
  const ageInputRef = useRef();

  const [enteredUsername, setEnteredUsername] = useState("");
  const [enteredAge, setEnteredAge] = useState("");
  const [error, setError] = useState();

  const handlerAddUser = (e) => {
    e.preventDefault();
    console.log(nameInputRef.current.value)
    if (enteredUsername.trim().length === 0 || enteredAge.trim().length === 0) {
      setError({
        title: "Invalid input",
        message: "Please enter a valid name and age (non-empty values).",
      });
      return;
    }
    if (+enteredAge < 1) {
      setError({
        title: "Invalid age",
        message: "Please enter a valid age (> 0)",
      });
      return;
    }
    console.log(enteredUsername, enteredAge);
    props.onAddUser({
      id: Math.random(),
      username: enteredUsername,
      age: enteredAge,
    });
    setEnteredUsername("");
    setEnteredAge("");
  };

  const handlerUsernameChanged = (e) => {
    setEnteredUsername(e.target.value);
  };

  const handlerAgeChanged = (e) => {
    setEnteredAge(e.target.value);
  };

  const handlerResetError = () => {
    setError();
  };

  return (
    <>
      {error && (
        <ErrorModal
          title={error.title}
          message={error.message}
          onConfirm={handlerResetError}
        />
      )}
      <Card className={styleAddUser.input}>
        <form onSubmit={handlerAddUser}>
          <label htmlFor="username">Username</label>
          <input
            id="username"
            type="text"
            onChange={handlerUsernameChanged}
            value={enteredUsername}
            ref={nameInputRef}
          />
          <label htmlFor="age">Age</label>
          <input
            id="age"
            type="number"
            onChange={handlerAgeChanged}
            value={enteredAge}
            ref={ageInputRef}
          />
          <Button type="submit">Add User</Button>
        </form>
      </Card>
    </>
  );
};

export default AddUser;
