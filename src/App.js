import { useState } from "react";

import AddUser from "./components/Users/AddUser";
import ListUser from "./components/Users/ListUser";

function App() {
  const initUserList = [
    {
      id: 1,
      username: "Derin",
      age: 26,
    },
  ];

  const [userList, setUserList] = useState(initUserList);

  const handlerAddNewUser = (newUser) => {
    setUserList((prev) => {
      return [...prev, newUser];
    });
  };

  return (
    <div>
      <AddUser onAddUser={handlerAddNewUser} />
      <ListUser users={userList} />
    </div>
  );
}

export default App;
